//
//  CitiesViewModel.swift
//  WeatherTest
//
//  Created by Moez bachagha on 28/6/2023.
//

import Foundation
class CitiesViewModel {
    private(set) var Cities: [City] = []
    private(set) var error: DataError? = nil
    
    private let apiService: CityAPILogic
    private let coreDatApiService: CityCoreViewModel
    
    
    init(apiService: CityAPILogic = CityAPI(), coreDatApiService : CityCoreViewModel = CityCoreViewModel() ) {
        self.apiService = apiService
        self.coreDatApiService =  coreDatApiService
    }
    
    func getCityDetails(lon : Double? , lat : Double? ,completion: @escaping( ([City]?, DataError?) -> () ) ) {
        apiService.getCityDetails(lon: lon, lat: lat,
            completion: { [weak self] result in
            switch result {
            case .success(let schools):
                self?.Cities = self?.Cities  ?? []
                completion(schools, nil)
            case .failure(let error):
                self?.error = error
                completion(nil, error)
            }
        })
    }
    
    
    func saveCityToCache (lon : Double? , lat : Double? ,completion: @escaping (_ rslt : Bool?) -> ()) {
        var city : City?
        
        getCityDetails(lon: lon, lat: lat,
            completion: { (result, _) in
            city = result?.first
            
       
        
       
        if city != nil {
            
            let cityModel : CityDataModel = CityDataModel( cityName: city?.name!, lon: city?.coord?.lon!, lat: city?.coord?.lat!, weatherDescription: city?.weather?.first?.description!, country: city?.sys?.country!, tempMin: city?.main?.temp_min!, tempMax: city?.main?.temp_max!)
            
            self.coreDatApiService.saveToCache(city: cityModel) {
                result in
                
                if result!.self {
                    completion(false)
                    
                }
                else {
                    completion(true)
                }
            }

        }
        })
    }
}

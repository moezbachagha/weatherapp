//
//  CityCoreDataViewModel.swift
//  WeatherTest
//
//  Created by Moez bachagha on 29/6/2023.
//

import Foundation
import CoreData

class CityCoreViewModel {
    
    private(set) var Cities: [City] = []
    var citiesArray: [NSManagedObject] = []
    private(set) var error: DataError? = nil
    
    private let apiService: CoreDataStackAPILogic
    
    
    
    init(apiService: CoreDataStackAPILogic = CoreDataStack()) {
        self.apiService = apiService
    }
    
    func saveToCache(city : CityDataModel, completion: @escaping( _ response :Bool?) -> () )  {
        apiService.saveDataCore(city: city) { result in
            let rslt   =  result?.value(forKey: "cityName")
            
            completion(rslt == nil)
            
            
        }
        
    }
    
    func getCitiesFromCache(completion: @escaping( ( _ cities : [NSManagedObject] ) -> () ) ) {
        apiService.fetchCities { result in
            
            if result != nil {
                
                self.citiesArray = result!
                completion(self.citiesArray)
            }
    
        }
    }

    
}

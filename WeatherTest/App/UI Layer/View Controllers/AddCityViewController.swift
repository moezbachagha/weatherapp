//
//  AddCityViewController.swift
//  WeatherTest
//
//  Created by Moez bachagha on 29/6/2023.
//

import UIKit
import MapKit


class AddCityViewController: UIViewController , MKMapViewDelegate{
    
    private let citiesViewModel: CitiesViewModel = CitiesViewModel()
    let alert = AlertService.shared
    var longitude : Double?
    var latitude : Double?

    @IBOutlet weak var mapView: MKMapView!
    
    @IBOutlet weak var latitudeTxt: UITextField!
    
    @IBOutlet weak var longitudeTxt: UITextField!
    @IBOutlet weak var addBtn: UIButton!
    
    
    @IBAction func saveCity(_ sender: Any) {
        
        addBtn.isEnabled = false
        
        citiesViewModel.saveCityToCache(lon: longitude, lat: latitude,
                    completion: { (result) in

            
            
            
            if !result!.self {
                
    
                self.alert.showAlert(title: "Alert", message: "City already added!.", viewController: self)

            }
            else {
                self.alert.showAlert(title: "Success", message: "City added!.", viewController: self)
            }
        } )
        
        addBtn.isEnabled = true

        }
        
        
    
    
    override func viewDidLoad() {
        
        super.viewDidLoad()

        mapView.delegate = self
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(handleMapTap(_:)))
          mapView.addGestureRecognizer(tapGesture)

        // Do any additional setup after loading the view.
    }
    
    @objc func handleMapTap(_ gestureRecognizer: UITapGestureRecognizer) {
        if gestureRecognizer.state == .ended {
            let tapPoint = gestureRecognizer.location(in: mapView)
            let coordinate = mapView.convert(tapPoint, toCoordinateFrom: mapView)

            addAnnotationAtCoordinate(coordinate)
        }
    }

    func addAnnotationAtCoordinate(_ coordinate: CLLocationCoordinate2D) {
        let annotation = MKPointAnnotation()
        annotation.coordinate = coordinate
        longitude = annotation.coordinate.longitude
        latitude = annotation.coordinate.latitude
        UserDefaults.standard.set(longitude, forKey: "longitude")
        UserDefaults.standard.set(latitude, forKey: "latitude")




        mapView.removeAnnotations(mapView.annotations)
        addBtn.isHidden = true
        mapView.addAnnotation(annotation)
    
        latitudeTxt.text = String(annotation.coordinate.latitude)
        longitudeTxt.text = String(annotation.coordinate.longitude)
        addBtn.isHidden = false
        print(annotation.description)
    }



}
/*    citiesViewModel.getCityDetails{ [weak self] (Cities, error) in
        if let error = error {
            let alert = UIAlertController(title: "Error",
                                          message: "Could not retrieve schools: \(error.localizedDescription)",
                                          preferredStyle: .alert)
            
            let action = UIAlertAction(title: "OK",
                                       style: .default)
            alert.addAction(action)
            self?.present(alert,
                          animated: true)
        }
        
        if let Cities = Cities {
            print(Cities.first?.name!)
        }
    }
 */

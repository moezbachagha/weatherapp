//
//  ViewController.swift
//  WeatherTest
//
//  Created by Moez bachagha on 28/6/2023.
//

import UIKit
import CoreData


class ViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    private let citiesViewModel: CitiesViewModel = CitiesViewModel()
    private let citiesCoredDataViewModel: CityCoreViewModel = CityCoreViewModel()
    var citiesArray: [NSManagedObject] = []

    
    
    @IBOutlet weak var citiesTable: UITableView!
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
                    return citiesArray.count

    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! UITableViewCell
                  let content = cell.viewWithTag(0)
                  let cityName = content!.viewWithTag(1) as! UILabel
                  let country = content!.viewWithTag(2) as! UILabel
                  let weatherDescription = content!.viewWithTag(3) as! UILabel
                  
        let cities = citiesArray[indexPath.row]
        cityName.text = cities.value(forKey: "cityName") as! String
        country.text = cities.value(forKey: "country") as! String
        weatherDescription.text = (cities.value(forKey: "weatherDescription") as! String)
 
                  return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        let cities = citiesArray[indexPath.row]
        
        let cityModel : CityDataModel = CityDataModel( cityName: cities.value(forKey: "cityName") as! String, lon: cities.value(forKey: "lon") as! Double, lat: cities.value(forKey: "lat") as! Double, weatherDescription: cities.value(forKey: "weatherDescription") as! String, country: cities.value(forKey: "country") as! String, tempMin: cities.value(forKey: "tempMin") as! Double, tempMax: cities.value(forKey: "tempMax") as! Double)
   

        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "CityDetailsViewController") as! CityDetailsViewController
        controller.city  = cityModel
        self.navigationController?.pushViewController(controller, animated:true)


      }
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            let context = appDelegate.persistentContainer.viewContext
            
            let favoriToDelete =  citiesArray[indexPath.row]
            context.delete(favoriToDelete)
            
            do{
                try context.save()
                citiesArray.remove(at: indexPath.row)
                citiesTable.deleteRows(at: [indexPath], with: .fade)
                //tableView.reloadData()
            }catch{
                print("Error")
            }
            
            
            
        }
    }

    
    override func viewWillAppear(_ animated: Bool) {
          super.viewWillAppear(animated)
            
        citiesCoredDataViewModel.getCitiesFromCache
        {
            result in
            self.citiesArray = result
            self.citiesTable.reloadData()

   
        }
     

      }
    override func viewDidLoad() {
        super.viewDidLoad()
   
    }


}


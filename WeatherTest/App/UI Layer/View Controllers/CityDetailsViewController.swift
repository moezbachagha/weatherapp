//
//  CityDetailsViewController.swift
//  WeatherTest
//
//  Created by Moez bachagha on 29/6/2023.
//

import UIKit

class CityDetailsViewController: UIViewController {
    
    var city  : CityDataModel?
    
    
    @IBOutlet weak var cityNameTxt: UILabel!
    @IBOutlet weak var countryNameTxt: UILabel!
    @IBOutlet weak var minTempTxt: UILabel!
    @IBOutlet weak var maxTempTxt: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        cityNameTxt.text = city?.cityName
        countryNameTxt.text = city?.country
        minTempTxt.text = String(format: "%.2f", city?.tempMin! ?? "0.0") + " °F"
        maxTempTxt.text = String(format: "%.2f", city?.tempMax! ?? "0.0") + " °F"

    }
    



}

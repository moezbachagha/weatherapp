//
//  CityDataModel.swift
//  WeatherTest
//
//  Created by Moez bachagha on 29/6/2023.
//

import Foundation
import Foundation

struct CityDataModel    {
    
    
    let cityName: String?
    let lon: Double?
    let lat: Double?
    let weatherDescription: String?
    let country: String?
    let tempMin : Double?
    let tempMax : Double?
   


}

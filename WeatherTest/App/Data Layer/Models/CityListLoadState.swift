//
//  CityListLoadState.swift
//  WeatherTest
//
//  Created by Moez bachagha on 28/6/2023.
//

import Foundation

enum CityListLoadState {
    case error
    case loaded
    case empty
}

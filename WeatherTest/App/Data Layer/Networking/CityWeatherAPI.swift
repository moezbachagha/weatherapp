//
//  CityWeatherAPI.swift
//  WeatherTest
//
//  Created by Moez bachagha on 28/6/2023.
//

import Foundation
import Alamofire

typealias SchoolListAPIResponse = (Swift.Result<[City]?, DataError>) -> Void

// API interface to retrieve city

protocol CityAPILogic {
    func getCityDetails(lon : Double? , lat:Double?, completion: @escaping (SchoolListAPIResponse))
}

class CityAPI: CityAPILogic {



    func getCityDetails(lon : Double? , lat : Double?, completion: @escaping (SchoolListAPIResponse)) {
        
        let param: [String:Any] = [
            
         "lat": lat ?? 0.0,
         "lon": lon ?? 0.0,
         "appid" : Common.Global.apiKey
         
     ]
        
       
        AF.request(Common.Global.LOCAL , method: .get, parameters: param, encoding : URLEncoding.default, headers: Common.Global.headers)
            .validate()
            .responseDecodable(of: City.self) { response in
                
                switch response.result {
                case .failure(let error):
                    completion(.failure(.networkingError(error.localizedDescription)))
                case .success(let cities):
                    completion(.success([cities]))
                }
            }
    
 
        
    }
    
}

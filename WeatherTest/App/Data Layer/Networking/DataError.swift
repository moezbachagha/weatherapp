//
//  DataError.swift
//  WeatherTest
//
//  Created by Moez bachagha on 28/6/2023.
//

import Foundation

enum DataError: Error {
    case networkingError(String)
}

//
//  CoreDataStack.swift
//  WeatherTest
//
//  Created by Moez bachagha on 29/6/2023.
//

import Foundation
import CoreData
import UIKit

typealias NewCityAPIResponse = (NSManagedObject?) -> Void
typealias CityListAPIResponse = ([NSManagedObject]?) -> Void



protocol CoreDataStackAPILogic {
    func saveDataCore(city : CityDataModel, completion: @escaping (NewCityAPIResponse))


func fetchCities(completion: @escaping (CityListAPIResponse))
}

class CoreDataStack : CoreDataStackAPILogic {
    
    
    let alert = AlertService.shared
    static let shared = CoreDataStack()
    var citiesArray: [NSManagedObject] = []


    func saveDataCore(city : CityDataModel ,completion: @escaping (NewCityAPIResponse) ) {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let persistantContainer = appDelegate.persistentContainer
        let context = persistantContainer.viewContext
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "CityData")
        
        
        request.predicate = NSPredicate(format: "cityName == %@", city.cityName!)
        
        
        
        do {
            let resultArray = try context.fetch(request)
            if resultArray.count == 0 {
                let cityDescription = NSEntityDescription.entity(forEntityName: "CityData", in: context)
                
                var newCity = NSManagedObject (entity: cityDescription!, insertInto: context)
               

                
                newCity.setValue(city.cityName!, forKey: "cityName")
                newCity.setValue(city.country!, forKey: "country")
                newCity.setValue(city.lat!, forKey: "lat")
                newCity.setValue(city.lon!, forKey: "lon")
                newCity.setValue(city.weatherDescription!, forKey: "weatherDescription")
                newCity.setValue(city.tempMin!, forKey: "tempMin")
                newCity.setValue(city.tempMax!, forKey: "tempMax")
                
                
                
                do {
                    try context.save()
                    print ("City Added !!")
                    completion(newCity)

                } catch {
                    print("Error !")
                }
            }else{
                print(resultArray.count)
                print ("City already Added !!")
                completion(nil)
            }
        } catch {
            print("error")
        }
        
    }
    /*******************************************************************************************************************************/
    
    func fetchCities(completion: @escaping (CityListAPIResponse)) {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let persistantContainer = appDelegate.persistentContainer
        let context = persistantContainer.viewContext
        let request = NSFetchRequest<NSManagedObject>(entityName: "CityData")
        
               
               do {
                   citiesArray = try context.fetch(request)
                   completion(citiesArray)

               } catch  {
                   print ("Error!")
               }
           }
    
}
